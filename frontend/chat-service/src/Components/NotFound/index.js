import React from 'react';


export default function NotFound(){
    return (
        <div>
            <p>Página não encontrada ¯\_(ツ)_/¯</p>
        </div>
    );
}