import React from 'react';

import UsersList from './UsersList';
import ChannelsList from './ChannelsList';
import ChatComponent from './ChatComponent';

export default function Chat(){
  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col">
            <ChatComponent/>
        </div>
      </div>
      <br></br>
      <div className="row justify-content-between">
        <UsersList/>
        <br></br>
        <ChannelsList/>
      </div>
    </div>
  );
}