import React, { useState } from 'react';

export default function UsersList(){
  const users = [] 
  return (
    <div className="col">
      <div className="card">
        <div className="card-header">
          <b>Usuários</b>
        </div>
        <div className="list-group list-group-flush">
          {
            users.length > 0 ?
            users.map((user, index) => {
              return (
                <div className="list-group-item" key={index}>
                  <div>
                    {user.username} -
                    <em className={user.online ? "text-success" : "col text-danger"}>
                      {user.online ? "  Online" : "  Offline"}
                    </em>
                  </div>
                </div>
              )}
            )
            :
            <div className="list-group-item">
              <div>
                Nenhum usuário foi encontrado.
              </div>
            </div>
          }
        </div>
      </div>
    </div>
  );
}