import React from 'react';

export default function ChannelsList(){
  const channels = [] 
  return (
    <div className="col">
      <div className="card">
        <div className="card-header">
          <b>Canais</b>
        </div>
        <div className="list-group list-group-flush">
          {
            channels.length > 0 ?
            channels.map((channel, index) => {
              return (
                <div className="list-group-item" key={index}>
                  <div>
                    {channel.title}
                  </div>
                </div>
              )}
            )
            :
            <div className="list-group-item">
              <div>
                Nenhum canal foi encontrado.
              </div>
            </div>
          }
        </div>
      </div>
    </div>
  );
}