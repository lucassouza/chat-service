import React from 'react';
import useForm from '../../Hooks/useForm';
import { chatSocket } from '../../WebSockets/chatRooms';

export default function ChatComponent(){
  const [{ values }, handleChange, handleSubmit] = useForm();

  const sendMessage = () => {
    chatSocket.send(JSON.stringify({
        'message': values.message
    }));
  }

  chatSocket.onmessage = (event) => {
    const data = JSON.parse(event.data);
    document.querySelector('#chat-log').value += (data.message + '\n');
  };

  return (
    <div className="col">
        <textarea id="chat-log" cols="100" rows="20"></textarea>
        <br></br>
        <form onSubmit={handleSubmit(sendMessage)}>
          <div className="row justify-content-between">
            <div className="col-4">
              <input onChange={handleChange} name="message" type="text" size="95"/>
            </div>
            <div className="col-3">
              <button type="submit">Send</button>
            </div>
          </div>
        </form>
    </div>
  );
}