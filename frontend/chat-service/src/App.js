import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import Chat from "./Components/Chat"
import Login from "./Components/Login";
import NotFound from "./Components/NotFound"


export default function App() {
  return (
    <Router>
      <div>
        <Routes>
          <Route exact path="/login" element={ <Login/> }/>
          <Route exact path="/" element={ <Chat/> }/>
          <Route path="*" element={ <NotFound/> }/>
        </Routes>
      </div>
    </Router>
  );
}
