## Serviço de Chat em Tempo Real

Este repositório visa o desenvolvimento e manutenção de um serviço de chat em tempo real.

## Funcionalidades

- Envio e recebimento de mensagens em tempo real

## Tecnologias Utilizadas:

### Conteinerização
- [Docker](https://www.docker.com/)
- [Docker-Compose](https://docs.docker.com/compose/)

### Frontend
- Linguagem JavaScript
- Framework [React](https://pt-br.reactjs.org/)

### Backend
- Linguagem Python
- [Framework Django](https://www.djangoproject.com/)
- [Django-Chanels](https://channels.readthedocs.io/en/stable/)
  - Para a implementação de WebSockets.
- [Redis](https://redis.io/)
  - Para o armazenamento das mensagens exibidas no canal/grupo.
- [Flake8](https://pypi.org/project/flake8/)
  - Para padronização de código.

## Build

### Backend
  - No `backend/docker-compose.yml`, altere a chave `REDIS_HOST` para o seu IP local.
    - Exemplo: `REDIS_HOST=192.0.0.1`
  - Navegue para a pasta `backend`
  - Execute o comando
  ```
  $ docker-compose up --build
  ```
  - O docker-compose realizará a build de todas as dependências do backend.
  - Se tudo ocorrer bem você obterá a seguinte mensagem em seu terminal:
  ```
    chat_1   | Watching for file changes with StatReloader
    chat_1   | Performing system checks...
    chat_1   | 
    chat_1   | System check identified no issues (0 silenced).
    chat_1   | January 25, 2022 - 05:10:12
    chat_1   | Django version 3.0, using settings 'settings'
    chat_1   | Starting ASGI/Channels version 3.0.4 development server at http://0.0.0.0:5050/
    chat_1   | Quit the server with CONTROL-C.
  ```

### Frontend
  - Navegue para a pasta `frontend/chat-service`
  - Instale os pacotes necessários:
  ```
    $ yarn install
  ```
  - Execute o frontend
  ```
   $ yarn start
  ```
  - Se a build ocorrer corretamente, você será redirecionado para a página principal.

## Utilizando o Chat
  - Em um navegador acesse `http://localhost:5000`
    - Cada aba acessando a página possui um `AnonymousUser`.
  - Você poderá testar o envio de mensagens através de 2 ou mais abas diferentes.

## Material de Apoio
- [Padrões de Commit](https://medium.com/linkapi-solutions/conventional-commits-pattern-3778d1a1e657)
- [Django-Channel Chat Tutorial](https://channels.readthedocs.io/en/stable/tutorial/index.html)
- [Simplificando formulários no React com Hooks](https://joaopedro.dev/simplificando-formularios-com-hooks/)
