from django.db import models


class User(models.Model):
    """
    User instance model.
    """

    username = models.CharField(unique=True, max_length=30)
    """User's identification."""

    first_name = models.CharField(max_length=30)
    """User's first name."""

    last_name = models.CharField(max_length=30)
    """User's last name."""
