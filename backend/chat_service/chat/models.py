from django.db import models

from users.models import User


class Channel(models.Model):
    """
    Channel instance model.
    """

    title = models.CharField(max_length=30)
    """Channel's presentation title."""

    users = models.ManyToManyField(User)
    """Registered users set."""


class Message(models.Model):
    """
    Message instance model.
    """

    text = models.CharField(max_length=255)
    """Body of the message."""

    sent_at = models.DateTimeField(auto_now_add=True, blank=True)
    """Datetime that's the message was sent."""

    sent_by = models.ForeignKey(User, on_delete=models.CASCADE)
    """User that's the author of the message."""

    channel = models.ForeignKey(Channel, on_delete=models.PROTECT)
    """Channel on which the message was posted."""
